# -*- coding: utf-8 -*-
"""
Created on Thu Apr 11 12:00:24 2024

@author: nle
"""
from datetime import datetime
import win32com.client
import time
import pandas as pd
import os
from tqdm import tqdm
from bpe_screening.stoxxs_bpe.src import weekly_bpe
#%% create file

index_tickers = ['SX5P', 'SX5E', 'SXXP', 'BKXP']
date_range = pd.bdate_range(end = datetime.today(), periods=5)
date = date_range[date_range.weekday==0][-1]
date_effect = date_range[date_range.weekday==4][-1].strftime('%d/%m/%Y')
date = date.strftime('%Y%m%d')  # datetime(2022, 4, 18)#
path = os.path.join(weekly_bpe.__file__, '../../data/clean/')
stoxx, file_path = weekly_bpe.create_file_weekly_BPE(index_tickers, date=date, path=path)

#%% create mail
to = 'julien.lebon-lafond@bpe.fr'
cc = ['majd.wassouf@bpe.fr', 'alexis.clisson@bpe.fr', 'ssureau@keplercheuvreux.com']
subject = 'Compositions %s'%date_effect
body = 'Bonjour, \n\nVoici les compositions indices du dernier vendredi %s\n\nCordialement,'%date_effect


ol=win32com.client.Dispatch("outlook.application")
olmailitem=0x0 #size of the new email
newmail=ol.CreateItem(olmailitem)
newmail.Subject= subject
newmail.To = to
newmail.CC = '; '.join([c for c in cc])
newmail.Body= body
newmail.Attachments.Add(file_path)
newmail.Display()

time_wait = 300
print("Delete file in %d seconds: %s"%(time_wait, file_path))
for i in tqdm(range(time_wait)):
    time.sleep(1)
os.remove(file_path)
