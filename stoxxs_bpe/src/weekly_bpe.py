# -*- coding: utf-8 -*-
"""
Created on Fri Mar 25 14:41:04 2022

@author: nle
"""
from datetime import datetime
import pandas as pd
import os
import matplotlib.pyplot as plt
import dbtools.src.get_repository as rep


def get_index_info(index_ticker, date):
    """
    Get index informations (D-1) from indexticker on a given date D
    -----------------------
    index_ticker: str, Bloomberg ticker of index
    date: str, %Y%m%d
    ------------
    Deturn DataFrame
            index: security_id
            columns: ['SECNAME', 'ISIN', 'WEIGHT', 'ICB_INDUSTRY', 'ICB_SUPERSECTOR']
    """

    df = rep.get_index_comp(index_ticker, date, date).T
    df.columns = ['weight']

    isin = rep.mapping_from_security(df.index, code='ISIN')
    secname = rep.mapping_from_security(df.index, code='SECNAME')
    sector = rep.mapping_sector_from_security(df.index, code='')
    sector = sector[['ICB_INDUSTRY', 'ICB_SUPERSECTOR']]
    df_merge = pd.concat([secname, isin, df, sector], axis=1) \
        .sort_values(['weight'], ascending=False)
    df_merge.columns = df_merge.columns.str.upper()
    return df_merge


def create_file_weekly_BPE(index_tickers, date=datetime.today().strftime('%Y%m%d'),
               path='W:/Global_Research/Quant_research/projets/screener/Factor_client/Excel/stoxxs bpe'):
    if not os.path.isdir(path):
        return ('Path does not exist: "%s"' % path)
    date_range = pd.date_range(end=date, periods=10)
    date_range = date_range[date_range.weekday<=4]

    index_names = rep.mapping_index_from_ticker(index_tickers, code = 'short_name')
    stoxx = {}
    keys = ['SECNAME', 'ISIN', 'WEIGHT', 'ECN', 'ICB_SUPERSECTOR', 'ICB_INDUSTRY']
    values = ['NAME', 'ISIN CODE', 'WEIGHT IN INDEX',
              'ECN SECTOR NAME', 'MKT SECTOR NAME', 'ICB INDUSTRY NAME']
    cols = dict(zip(keys, values))

    index_renames = pd.Series({'SX5P': 'DJ STOXX 50', 'SX5E': 'DJ ESTOXX50',
                               'SXXP': 'DJ STOXX 600', 'BKXP': 'DJ STOXX TM'})
    temp = pd.concat((index_names, index_renames))
    index_names = temp[~temp.index.duplicated(keep='last')]
    # monday = date_range[date_range.weekday == 0][-1].strftime('%Y%m%d')
    for index_ticker in index_tickers:
        print('--------- %s -------'%index_ticker)
        df = get_index_info(index_ticker, date) \
            .sort_values(['WEIGHT'], ascending=False)
        df['ECN'] = df['ICB_INDUSTRY']
        df = df[cols.keys()]
        if df.isna().sum().sum() != 0:
            data_na = df[df.isna().sum(axis=1) != 0]
            error = '%s: Missing Data of instruments:\n %s\n' % (index_ticker, data_na)
            filename = '%s/Error_%s.txt' % (path, datetime.today().strftime('%Y%m%d'))
            f = open(filename, "a")
            f.write(error)
            f.close()
            print(error)
            raise Exception(error)
        if df.WEIGHT.sum().round(1)!=100:
            error = "%s: Sum of weight is: %.4f (not equal to 100)\n" % (index_ticker, df.WEIGHT.sum())
            filename = '%s/Error_%s.txt' % (path, datetime.today().strftime('%Y%m%d'))
            f = open(filename, "a")
            f.write(error)
            f.close()
            print(error)
            raise Exception(error)

        df = df.rename(columns=cols)
        stoxx[index_names[index_ticker]] = df
    print("----------- Creating excel -----------")
    data_date = date_range[-2].strftime('%Y%m%d')
    path = '%s/stoxxs %s %s %s.xlsx' % (path, data_date[:4], data_date[4:6], data_date[6:])
    with pd.ExcelWriter(path) as writer:
        for key, value in stoxx.items():
            value.to_excel(writer, sheet_name=key, index=None)
    print('File save in: %s' % path)
    print("----------- Check plot -----------")
    df_wt = pd.DataFrame()
    fig, ax = plt.subplots(2,2)
    for i, k in enumerate(stoxx.keys()):
        df = stoxx[k].copy().reset_index()
        df = df['WEIGHT IN INDEX'].to_frame(k)
        df.cumsum().plot(ax = ax[i//2,i%2])
        df_wt = pd.concat((df_wt, df), axis =1)
        ax[i//2,i%2].grid()
    plt.show()
    return stoxx, path



