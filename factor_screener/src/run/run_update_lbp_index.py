# -*- coding: utf-8 -*-
"""
Created on Tue Dec  5 14:34:28 2023

@author: nle
"""

import os, sys
import pandas as pd
import numpy as np
from dbtools.src.db_connexion import SqlConnector
import dbtools.src.get_repository as rep
import dbtools.src.DatabaseManager as db
import backtest.src.backtest as bkt
from copy import deepcopy
import matplotlib.pyplot as plt

from bpe_screening.factor_screener.config.update_lbp_index_config import LBPConfig as Conf
from bpe_screening.factor_screener.src.update_lbp_index import (LBPIndexUpdater,
                                                 get_param_backtest, get_backtest_data, backtest,
                                                 get_index_ref, filter_existing_data, import_to_sql,
                                                 get_index_compo, update_compo_level, get_end_date,
                                                 recompute_index_level, filter_existing_data)


#%% param
end_date = ''
#%%
if end_date =='':
    end_date = get_end_date()

config_v3 = Conf.spec_lbp_v3
config_v4 = Conf.spec_lbp_v4
mapping_price_return_total_return = Conf.mapping_price_return_total_return
s_pr_tr = Conf.s_pr_tr
s_pr_tr_id = Conf.s_pr_tr_id
s_ticker_index_id = Conf.s_ticker_index_id
s_index_id_name = Conf.s_index_id_name

index_id = Conf.index_id

#%%

df_index_ref = get_index_ref(index_id)
df_index_ref.index = df_index_ref.index_id

#%% V3
lpb_idx = LBPIndexUpdater(config_v3)

l_files = lpb_idx.list_files()
l_files = l_files[l_files.index>df_index_ref.date_compo.min()]

df_ptf_v3 = lpb_idx.concatenate_excel(l_files)
df_new_compo_v3 = lpb_idx.transform_to_db_format(df_ptf_v3)
#%% V4
lpb_idx = LBPIndexUpdater(config_v4)

l_files = lpb_idx.list_files()
l_files = l_files[l_files.index>df_index_ref.date_compo.min()]

df_ptf_v4 = lpb_idx.concatenate_excel(l_files)
df_new_compo_v4 = lpb_idx.transform_to_db_format(df_ptf_v4)

#%%
df_new_compo = pd.concat((df_new_compo_v3, df_new_compo_v4)).drop_duplicates()


df_idx_lvl = update_compo_level(config_v3, df_new_compo, index_id, end_date, to_sql=False)
df_idx_lvl_rec = recompute_index_level(df_index_ref, df_idx_lvl)
df_idx_lvl_imp = filter_existing_data(df_idx_lvl_rec, df_index_ref.date_level)

import_to_sql(df_idx_lvl_imp, config_v3['table_level'])
import_to_sql(df_new_compo, config_v3['table_compo'])

#%%















