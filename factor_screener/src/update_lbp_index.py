# -*- coding: utf-8 -*-
"""
Created on Mon Nov 13 13:04:06 2023

@author: nle
"""

import os, sys
import pandas as pd
import numpy as np
from dbtools.src.db_connexion import SqlConnector
import dbtools.src.get_repository as rep
import dbtools.src.DatabaseManager as db
import backtest.src.backtest as bkt
from copy import deepcopy
import matplotlib.pyplot as plt
from pandas.tseries.offsets import BDay

from bpe_screening.factor_screener.config.update_lbp_index_config import LBPConfig as Conf


connector = SqlConnector()
con_mis = connector.connection()


mapping_price_return_total_return = Conf.mapping_price_return_total_return
s_pr_tr = Conf.s_pr_tr
s_pr_tr_id = Conf.s_pr_tr_id
s_ticker_index_id = Conf.s_ticker_index_id
s_index_id_name = Conf.s_index_id_name


#%%
class LBPIndexUpdater():
    def __init__(self, config):
        self.config = config
        self.df_index_ref = self.get_index_ref()
        self.max_date_compo = self.df_index_ref.date_compo.min().strftime('%Y%m%d')
        self.max_date_level = self.df_index_ref.date_level.min().strftime('%Y%m%d')

        pass
    def get_index_ref(self):
        
        config = self.config
        ticker = pd.Series(config.get('col_map', {}).values())
        ticker = s_pr_tr.loc[ticker]
        ticker = ticker.index.union(ticker).union(ticker.values)
        
        req = """select index_id, max(DATE) max_date
        from QUANT_work..INDEX_COMPOSITION
        group by index_id"""
        df = pd.read_sql(req, con_mis)
        s_max_date_compo = pd.Series(index = df.index_id.values, data = df.max_date.values)
        s_max_date_compo = pd.to_datetime(s_max_date_compo)
        
        req = """select a.index_id, a.DATE, a.value
                from QUANT_work..INDEX_LEVEL a inner join (
                select index_id, max(DATE) max_date
                from QUANT_work..INDEX_LEVEL
                group by index_id) b
                on a.index_id=b.index_id and a.DATE = b.max_date
                order by a.index_id
                """
        df_index_data = pd.read_sql(req, con_mis)
        df_index_data = df_index_data.set_index('index_id')

        df_index_ref = rep.mapping_index_from_ticker(ticker).to_frame('index_id')
        df_index_ref['date_compo'] = df_index_ref.index_id.map(s_max_date_compo).fillna(pd.to_datetime('19990101'))
        df_index_ref['date_compo'] = pd.to_datetime(df_index_ref['date_compo'])
        df_index_ref['date_level'] = df_index_ref.index_id.map(df_index_data.DATE).fillna(pd.to_datetime('19990101'))
        df_index_ref['date_level'] = pd.to_datetime(df_index_ref['date_level'])
        df_index_ref['index_level'] = df_index_ref.index_id.map(df_index_data.value).fillna(1)

        return df_index_ref

    def eom_of_given_date(self, date):
        eom = pd.to_datetime(date) + BDay(30)
        date_range = pd.date_range(start = date, periods = 30, freq = BDay())
        holidays = pd.read_sql('select * from QUANT_work..quant_holidays', con_mis)
        holidays = pd.to_datetime(holidays['date'].values)
        date_range = date_range.difference(holidays)
        eom = date_range[date_range.month==pd.to_datetime(date).month][-1]
        return eom

    def list_files(self):
        filename = self.config['filename']
        folder = self.config['folder']
        start_date = pd.to_datetime([self.config['start_date']]).max().strftime('%Y%m%d')
        list_path = pd.Series(dtype = 'object')

        for year in os.listdir(folder):
            path_year = os.path.join(folder, year)
            if not os.path.isdir(path_year):
                continue

            for rebal in os.listdir(path_year):
                
                path_rebal = os.path.normpath(os.path.join(path_year, rebal))
                f = filename.format(date=rebal)

                if not os.path.isdir(path_rebal):
                    continue
                if not (f in os.listdir(path_rebal)):
                    continue
                if pd.to_datetime(rebal) < pd.to_datetime(start_date):
                    continue

                f = os.path.normpath(os.path.join(path_rebal, f))
                eom = self.eom_of_given_date(rebal)
                list_path.loc[pd.to_datetime(eom)] = f
        return list_path
    
    def load_compo_from_excel(self, file_path):
        sheet_name = self.config['sheet_name']
        list_ptf = list_ptf = self.config['portfolios']
        
        df = pd.read_excel(file_path, sheet_name = sheet_name, index_col = 0)
        if 'member' in df.columns:
            df = df[df.member==1]
        df = df[df.columns.intersection(list_ptf)]
        df.index.name = 'security_id'
        df = df.dropna(how ='all', axis = 1)
        if 'MorningStar_CD' in df.columns:
            df['cyclical'] = (df['MorningStar_CD']=='C')*1
            df['defensive'] = (df['MorningStar_CD']=='D')*1
            df = df.drop(columns = ['MorningStar_CD'])

        if 'sales_pct' in df.columns:
            df['global'] = (df['sales_pct']=='GL')*1
            df['domestic'] = (df['sales_pct']=='DO')*1
            df = df.drop(columns = ['sales_pct'])
        df = df.replace({0:np.nan})
        print(df.columns)
        df['benchmark'] = 1
        df = df.div(df.sum())
        df = df.dropna(how = 'all', axis =1)
        return df

    
    def concatenate_excel(self, list_files):

        df_ptf = pd.DataFrame()
        for date in list_files.index:
            df_tmp = self.load_compo_from_excel(list_files.loc[date])
            df_tmp['DATE'] = date
            df_tmp.index.name = 'security_id'
            df_ptf = pd.concat((df_ptf, df_tmp))


        df_ptf = df_ptf.reset_index()
        if df_ptf.empty:
            df_ptf = pd.DataFrame(columns = ['DATE', 'security_id'])
        df_ptf = df_ptf.set_index(['DATE', 'security_id']).reset_index()

        return df_ptf

    def transform_to_db_format(self, df_ptf):
        df_index_ref = self.df_index_ref
        col_map = self.config['col_map']

        df_ptf = df_ptf.set_index(['DATE', 'security_id'])
        df_ptf = df_ptf[df_ptf.columns.intersection(col_map.keys())].rename(columns = col_map)
        df_ptf = df_ptf.rename(columns = df_index_ref.index_id)
        df_ptf.columns.name = 'index_id'
        df_ptf = df_ptf.stack().to_frame('SECURITYRATIO')
        df_ptf = df_ptf.reset_index()
        
        df_ptf = df_ptf.sort_values(by = ['DATE', 'index_id', 'security_id']).drop_duplicates()
        return df_ptf


def get_index_ref(index_id):
    
    ticker = s_ticker_index_id[s_ticker_index_id.isin(index_id)].index

    req = """select index_id, max(DATE) max_date
    from QUANT_work..INDEX_COMPOSITION
    group by index_id"""
    df = pd.read_sql(req, con_mis)
    s_max_date_compo = pd.Series(index = df.index_id.values, data = df.max_date.values)
    s_max_date_compo = pd.to_datetime(s_max_date_compo)
    
    req = """select a.index_id, a.DATE, a.value
            from QUANT_work..INDEX_LEVEL a inner join (
            select index_id, max(DATE) max_date
            from QUANT_work..INDEX_LEVEL
            group by index_id) b
            on a.index_id=b.index_id and a.DATE = b.max_date
            order by a.index_id
            """
    df_index_data = pd.read_sql(req, con_mis)
    df_index_data = df_index_data.set_index('index_id')

    df_index_ref = rep.mapping_index_from_ticker(ticker).to_frame('index_id')
    df_index_ref['date_compo'] = df_index_ref.index_id.map(s_max_date_compo).fillna(pd.to_datetime('19990101'))
    df_index_ref['date_compo'] = pd.to_datetime(df_index_ref['date_compo'])
    df_index_ref['date_level'] = df_index_ref.index_id.map(df_index_data.DATE).fillna(pd.to_datetime('19990101'))
    df_index_ref['date_level'] = pd.to_datetime(df_index_ref['date_level'])
    df_index_ref['index_level'] = df_index_ref.index_id.map(df_index_data.value).fillna(1)

    return df_index_ref


def get_end_date():
    month = pd.Timestamp.today().strftime('%Y%m') + '01'
    end_date = pd.to_datetime(month) - pd.Timedelta('1D')
    return end_date

def get_param_backtest(df_compo):
    dict_compo = {}
    security_id = pd.Index([])
    df_date = pd.DataFrame(columns = ['start_date', 'end_date', 'begin_date'])

    for idx_id in df_compo.index_id.drop_duplicates():
        df_index = df_compo[df_compo.index_id==idx_id].copy()
        df_compo_tmp = df_index.set_index(['DATE', 'security_id']).SECURITYRATIO.unstack()
        dict_compo[idx_id] = df_compo_tmp.copy()
        security_id = security_id.union(df_compo_tmp.columns)
        start_date = df_compo_tmp.index.min() - pd.Timedelta('15D')
        begin_date = df_compo_tmp.index.min()
        end_date = get_end_date()
        df_date.loc[idx_id, ['start_date', 'end_date', 'begin_date']] =  [start_date, end_date, begin_date]
    start_date = df_date['start_date'].min()
    begin_date = df_date['begin_date'].min()
    end_date = df_date['end_date'].max()
    
    params = {'end_date':end_date,
              'start_date':start_date,
              'begin_date':begin_date,
              'security_id':security_id,
              'index_ticker' : [],
              'dict_compo' : deepcopy(dict_compo)
              }
    return params

def get_backtest_data(params, end_date = '', rebalance = 'monthly'):
    start_date = params.get('start_date')
    begin_date = params.get('begin_date')
    if end_date=='':
        end_date = params.get('end_date')
    security_id = params.get('security_id')
    index_ticker = params.get('index_ticker', [])
    rebal_dates = bkt.Rebalance(begin_date, end_date, rebalance).rebal_date
    rebal_dates = pd.Index(rebal_dates.values)

    backtest_data = bkt.BacktestData(start_date=start_date, end_date=end_date, begin_date=begin_date,
                                 index_ticker=index_ticker, security_id=security_id,
                                 flag_fundamental_data = False)

    data_bkt = deepcopy(backtest_data.data_bkt)
    
    for k in ['flag_lms', 'close_prc_total', 'close_prc', 'index_prc']:
        df = data_bkt[k].copy()
        index = df.index.union(rebal_dates)
        df = df.reindex(index = index, method = 'ffill')
        data_bkt[k] = df.copy()
    
    dict_compo = params.get('dict_compo')
    dict_target = {}
    for k in dict_compo.keys():
        df = dict_compo[k].reindex(data_bkt['close_prc'].index, method = 'ffill')
        df = df.loc[rebal_dates]
        dict_target[k] = df.copy()
    data_bkt['target_weight'] = deepcopy(dict_target)
    return deepcopy(data_bkt)

def backtest(data_bkt):
    dict_target = deepcopy(data_bkt['target_weight'])
    ptf_analysis = bkt.PortfolioAnalysis(data_bkt, dict_target)

    df_perf = deepcopy(ptf_analysis.df_perf)
    df_perf_gross = deepcopy(ptf_analysis.df_perf_gross)
    df_perf_gross = df_perf_gross.rename(columns = s_pr_tr_id)
    df_perf_gross = df_perf_gross[df_perf_gross.columns.difference(df_perf.columns)]
    
    df_perf_ts = pd.concat((df_perf, df_perf_gross), axis = 1)
    df_perf_db = df_perf_ts.stack()
    df_perf_db = df_perf_db.reset_index()
    df_perf_db.columns = ['DATE', 'index_id', 'value']
    df_perf_db = df_perf_db.sort_values(by = ['index_id', 'DATE'])
    df_perf_db = df_perf_db.set_index(['index_id', 'DATE'])
    df_perf_db = df_perf_db.reset_index()

    return df_perf_db


def recompute_index_level(df_index_ref, df_perf_db):

    s_level_date = pd.Series(index = df_index_ref.index_id.values, data = df_index_ref.date_level.values)
    s_inception_value = df_index_ref.set_index('index_id').index_level
    
    df = df_perf_db.copy()
    df['date_level'] = df.index_id.map(s_level_date)
    df = df[df.DATE>=df['date_level']]

    init_value = df.sort_values(by = ['index_id', 'DATE']).drop_duplicates(['index_id'], keep='first')
    init_value = init_value.set_index('index_id').value
    factor = init_value/s_inception_value
    
    df['factor'] = df.index_id.map(factor)
    df['value_recomputed'] = df['value']/df['factor']
    df = df.drop(columns = ['date_level', 'date_level', 'value', 'factor'])
    df = df.rename(columns = {'value_recomputed':'value'})
    return df

def filter_existing_data(df_db, s_start_date):
    df = df_db.copy()
    df['start_date'] = df.index_id.map(s_start_date)
    df = df[df['DATE']>df['start_date']]
    df = df.drop(columns = ['start_date'])
    return df.copy()

def import_to_sql(df, table_name):

    db_quant_index = db.DatabaseManager(table_name)
    db_quant_index.append_table(df = df.copy())
    pass


def get_index_compo(index_id, start_date, end_date):
    sec_str = ','.join([str(int(s)) for s in index_id])
    req = """
     select DATE, index_id, security_id, SECURITYRATIO from QUANT_work..INDEX_COMPOSITION
     where index_id in (%s)
     and DATE>='%s' and DATE<='%s'
    """ %(sec_str, start_date, end_date)
    df = pd.read_sql(req, con_mis)
    df.DATE = pd.to_datetime(df.DATE)
    return df

def get_index_data(index_id, start_date, end_date):
    sec_str = ','.join([str(int(s)) for s in index_id])
    req = """
     select DATE, index_id, value from QUANT_work..INDEX_LEVEL
     where index_id in (%s)
     and DATE>='%s' and DATE<='%s'
     
    """ %(sec_str, start_date, end_date)
    df = pd.read_sql(req, con_mis)
    df = df.set_index(['DATE', 'index_id']).value.unstack()
    df.index.name = None
    df.columns.name = None
    return df

def update_compo_level(config, df_new_compo, index_id, end_date, to_sql = False):
    df_index_ref = get_index_ref(index_id)
    df_index_ref.index = df_index_ref.index_id
    df_old_compo = get_index_compo(index_id,
                                   start_date = df_index_ref.date_level.min(),
                                   end_date = df_index_ref.date_compo.max())
    df_old_compo = df_old_compo[df_old_compo.index_id.isin(df_new_compo.index_id.unique())]
    df_compo = pd.concat((df_old_compo, df_new_compo)).drop_duplicates(['DATE', 'security_id', 'index_id']).dropna()
    
    df_compo_gr = df_compo.copy()
    df_compo_gr.index_id = df_compo_gr.index_id.replace(s_pr_tr_id)


    params = get_param_backtest(df_compo)
    print(params)
    if params['end_date']> pd.to_datetime(end_date):
        print('Table %s, %s are up to date %s' %(config['table_level'], config['table_compo'], end_date))
        return pd.DataFrame()
    data_bkt = get_backtest_data(params, end_date)

    df_perf_db = backtest(data_bkt)
    
    if to_sql:
        df_idx_lvl = recompute_index_level(df_index_ref, df_perf_db)
        df_idx_lvl = filter_existing_data(df_idx_lvl, df_index_ref.date_level)
        
        df_compo_tmp = pd.concat((df_compo, df_compo_gr))
        df_compo_tmp.SECURITYRATIO = df_compo_tmp.SECURITYRATIO*100
        df_compo_tmp = filter_existing_data(df_compo_tmp, df_index_ref.date_compo)
    
        import_to_sql(df_idx_lvl, config['table_level'])
        import_to_sql(df_compo_tmp, config['table_compo'])
    
    return df_perf_db


def analyze_index(df_index, df_ffama = pd.DataFrame()):
    idx = pd.Index(df_index.columns.drop_duplicates())
    idx = idx.union(s_pr_tr_id.loc[s_pr_tr_id.index.intersection(idx).values])


    df_index_perf = pd.DataFrame()
    for i in idx:

        index_gr = s_pr_tr_id.loc[s_pr_tr_id.index.intersection([i])]
        if index_gr.empty:
            index_gr = i
        else:
            index_gr = index_gr.iloc[0]
        df_idx = df_index[[i,index_gr]]
        df_idx.columns = ['price_return', 'gross_return']
        
        df_perf_ana = bkt.analyze_performance(df_idx, df_ffama = df_ffama, yearly = True)
        df_perf_ana.index.name = 'period'
        df_perf_ana = df_perf_ana.reset_index()
        df_perf_ana['index_id'] = i
        df_perf_ana = df_perf_ana.set_index(['index_id','period'])
        
        df_index_perf = pd.concat((df_index_perf, df_perf_ana))
    return df_index_perf
# =============================================================================
# #%%
# config = deepcopy(config)
# start_date = '20210301'
# end_date = '20231130'
# index_id = [95, 96, 97, 98, 99, 100, 101, 146]
# df_compo_db = pd.DataFrame()
# df_compo_db['index_id'] = index_id
# 
# df_old_compo = get_index_compo(index_id, start_date, end_date)
# 
# df_idx_lvl = update_compo_level(config, df_old_compo, end_date)
# =============================================================================

#%%%%%%%%%%%%%%%%%% initialize table INDEX_COMPONENT for Beta profiles %%%%%%%%%%%%%%%%%%
# =============================================================================
# config = deepcopy(config)
# start_date = '20210301'
# end_date = '20231130'
# 
# 
# kc_idx = KechIndexUpdater(config)
# l_files = kc_idx.list_files()
# l_files = l_files[l_files.index>=start_date]
# 
# 
# df_ptf = kc_idx.concatenate_excel(l_files)
# 
# 
# df_compo_db = pd.DataFrame()
# 
# df_old_compo = kc_idx.transform_to_db_format(df_ptf)
# 
# 
# 
# df_compo = pd.concat((df_old_compo, df_compo_db)).drop_duplicates()
# 
# df_compo_gr = df_compo.copy()
# df_compo_gr.index_id = df_compo_gr.index_id.replace(s_pr_tr_id)
# 
# 
# params = get_param_backtest(df_compo)
# data_bkt = get_backtest_data(params, end_date)
# 
# df_perf_db = backtest(data_bkt)
# 
# #%% import to sql
# df_index_ref = get_index_ref(config)
# df_index_ref.index = df_index_ref.index_id
# df_compo_tmp = pd.concat((df_compo, df_compo_gr))
# df_compo_tmp = filter_existing_data(df_compo_tmp, df_index_ref.date_compo)
# import_to_sql(df_compo_tmp, config['table_compo'])
# import_to_sql(df_perf_db, config['table_level'])
# =============================================================================
#%% performance index
# =============================================================================
# df_idx = df_perf_db.set_index(['DATE','index_id']).value.unstack()
# df_idx.columns.name = None
# 
# 
# df_index_perf = analyze_index(df_idx)
# #%% Benchmark Index
# from kc_bbg_api.connection import ApiSettings,KCBBGClient
# from kc_bbg_api import historical_data,intraday_tick_data,reference_data,indexcomposition_data,intraday_bar_data
# 
# ApiSettings.user_name = "bbg.quant-research.user"
# ApiSettings.user_password = "pZ_B5LJ-QgfOO0Vpk_Yq"
# 
# start_date = df_perf_db.DATE.min()# '20221003'#20160614
# end_date = df_perf_db.DATE.max() #'20221015'#20160620
# 
# bck_idx = list(pd.Index(['SV2P', 'SV2R', 'SG2P', 'SG2R', 'SXSQEP','SD3P', 'SD3R','M9EUEV','MXEUQU','MXEUHDVD']) + ' Index')
# df_bck_idx = historical_data(securities=bck_idx,
#                     fields=["PX_LAST"],
#                     startDate=start_date, endDate=end_date
#                     )
# df_bck_idx = df_bck_idx.set_index(['date', 'security'])['PX_LAST'].unstack()
# df_bck_ret = df_bck_idx.pct_change(1).fillna(0)
# df_bck_idx = (df_bck_ret + 1).cumprod()
# 
# 
# #print(df_bck_idx.set_index('date')['PX_LAST'].pct_change(-1))
# 
# #%% plot
# df_idx_name = df_idx.rename(columns = s_index_id_name)
# 
# df_idx_name = df_idx_name.merge(df_bck_idx, left_index = True, right_index = True)
# 
# df_idx_gr = df_idx_name[df_idx_name.columns[df_idx_name.columns.str.contains(' GR')]]
# 
# df_idx_pr = df_idx_name[df_idx_name.columns[~df_idx_name.columns.str.contains(' GR')]]
# 
# #%%
# df_idx_gr[['KC Value GR', 'KC Growth GR', 'KC Coverage GR']].plot()
# plt.grid()
# plt.show()
# 
# df_idx_gr[['KC Low vol GR', 'KC High vol GR', 'KC Coverage GR']].plot()
# plt.grid()
# plt.show()
# 
# df_idx_gr[['KC Quality GR', 'KC Momentum GR', 'KC Coverage GR', 'KC Dividend GR']].plot()
# plt.grid()
# plt.show()
# 
# 
# #%%
# df_value = df_idx_name[['KC Value', 'KC Value GR', 'SV2P Index', 'SV2R Index', 'M9EUEV Index']]
# df_value.plot()
# plt.grid()
# plt.show()
# 
# df_growth = df_idx_name[['KC Growth', 'KC Growth GR', 'SG2P Index', 'SG2R Index']]
# df_growth.plot()
# plt.grid()
# plt.show()
# 
# df_dvd = df_idx_name[['KC Dividend', 'KC Dividend GR', 'SD3P Index', 'MXEUHDVD Index']]
# df_dvd.plot()
# plt.grid()
# plt.show()
# 
# df_quality = df_idx_name[['KC Quality', 'KC Quality GR', 'MXEUQU Index']]
# df_quality.plot()
# plt.grid()
# plt.show()
# 
# 
# df_perf_pr = analyze_index(df_idx_pr)
# =============================================================================
#%% Update compo and Index level
# =============================================================================
# end_date = ''
# 
# # composition fb
# df_new_compo = load_compo_from_excel(spec_fb, 'C:/python_projects/family_business/data/clean/compo/20230915.xlsx')
# df_new_compo['DATE'] = pd.to_datetime('20230915')
# 
# _ = update_compo_level(spec_fb, df_new_compo, end_date)
# 
# # Composition KECH
# DATE = '20230915'
# df_kc_cov = rep.get_kc_coverage_histo(DATE)
# df_compo_kc = df_kc_cov[['security_id']]
# df_compo_kc['index_id'] = s_ticker_index_id['KCCOVEWP']
# df_compo_kc['SECURITYRATIO'] = 100/len(df_compo_kc)
# df_compo_kc['DATE'] = pd.to_datetime(DATE)
# 
# 
# _ = update_compo_level(spec_fb, df_compo_kc, end_date)
# 
# #%% Analyse index
# 
# df_lvl, df_perf = analyze_index([91, 93, 146], '20180615', '20231110')
# 
# df_lvl = df_lvl.rename(columns = s_index_id_name)
# df_perf = df_perf.rename(index = s_index_id_name)
# 
# print('Check performance: df_perf')
# print('Check index level: df_lvl')
# 
# # plot
# df_lvl[df_lvl.columns[df_lvl.columns.str.contains('GR')]].plot()
# plt.grid()
# plt.show()
# 
# =============================================================================























