# -*- coding: utf-8 -*-
"""
Created on Fri Oct 13 10:53:59 2023

@author: nle
"""

import pandas as pd
import dbtools.src.get_repository as rep

class LBPConfig:
    
    mapping_price_return_total_return = {
        'KCLBPEWP': 'KCLBPEWGR',
        'KCLBP3HMP': 'KCLBP3HMGR',
        'KCLBP3LMP':'KCLBP3LMGR',
        'KCLBP3HOP':'KCLBP3HOGR',
        'KCLBP3LOP':'KCLBP3LOGR',
        'KCLBP3HQP':'KCLBP3HQGR',
        'KCLBP3LQP':'KCLBP3LQGR',
        'KCLBP4DMP':'KCLBP4DMGR',
        'KCLBP4GLP':'KCLBP4GLGR',
        'KCLBP4CP':'KCLBP4CGR',
        'KCLBP4DFP':'KCLBP4DFGR',
        'KCLBP4GP':'KCLBP4GGR',
        'KCLBP4VP':'KCLBP4VGR',
        'KCLBP4HMP':'KCLBP4HMGR',
        'KCLBP4LMP':'KCLBP4LMGR',
        'KCLBP4HQP':'KCLBP4HQGR',
        'KCLBP4LQP':'KCLBP4LQGR',
        'KCLBP4DMP':'KCLBP4DMGR',
        'KCLBP4GLP':'KCLBP4GLGR',
        'KCLBP4CP':'KCLBP4CGR',
        'KCLBP4DFP':'KCLBP4DFGR',
        'KCLBP4LRP':'KCLBP4LRGR',
        'KCLBP4HRP':'KCLBP4HRGR',
        'KCBETA2HVP':'KCBETA2HVGR',
        'KCBETA2LVP':'KCBETA2LVGR',
        'KCBETA2GP':'KCBETA2GGR',
        'KCBETA2VP':'KCBETA2VGR',
        'KCBETA2MP':'KCBETA2MGR',
        'KCBETA2DP':'KCBETA2DGR',
        'KCBETA2QP':'KCBETA2QGR',
        'KCCOVEWP':'KCCOVEWGR',
        'KCFB25P':'KCFB25GR',
        'KCFB50P':'KCFB50GR',
        'SXXP':'SXXGR',
        'MXEU000V':'M7EU000V',
        'MXEU000G':'M7EU000G',
        'MXEUHDVD':'M7EUHDVD',
        'MXEUQU':'M7EUQU',
        'MXEUMVOL':'M3EUMINV',
        'M3EUMMT':'MAEUMMT',
        'MXEU':'M7EU',
        'STVP':'STVR',
        'STGP':'STGR',
        'SXXEWP':'SXXEWR',
        'SV2P':'SV2R',
        'SG2P':'SG2R',
        'SD3P':'SD3R',
        'BKXP':'BKXP'
    }

    spec_lbp_v3={
            'folder': 'W:/Global_Research/Quant_research/projets/screener/Factor_client/Publication/',
            'start_date':'20220101',
            'col_map' : {'benchmark':'KCLBPEWP',
                          'momentum_high':'KCLBP3HMP',
                          'momentum_low':'KCLBP3LMP',
                          'oppo_high':'KCLBP3HOP',
                          'oppo_low':'KCLBP3LOP',
                          'quality_high':'KCLBP3HQP',
                          'quality_low':'KCLBP3LQP',
                          'domestic':'KCLBP4DMP',
                          'global':'KCLBP4GLP',
                          'cyclical':'KCLBP4CP',
                          'defensive':'KCLBP4DFP'},
            'filename' : 'Breakdown_{date}_v31_BPE.xlsx',
            'sheet_name' : 'Summary_complete',
            'portfolios' : pd.Index(['MorningStar_CD','sales_pct','highvol','lowvol','quality_high','quality_low','growth','value',
                                 'momentum_high','momentum_low', 'high_risk', 'low_risk', 'oppo_high', 'oppo_low']),
            'table_compo':'INDEX_COMPOSITION',
            'table_level':'INDEX_LEVEL',
            'rebalance': 'monthly',
    }

    spec_lbp_v4={
            'folder': 'W:/Global_Research/Quant_research/projets/screener/Factor_client/Publication/',
            'start_date':'20220101',
            'col_map' : {'benchmark':'KCLBPEWP',
                          'growth':'KCLBP4GP',
                          'value':'KCLBP4VP',
                          'momentum_high':'KCLBP4HMP',
                          'momentum_low':'KCLBP4LMP',
                          'quality_high':'KCLBP4HQP',
                          'quality_low':'KCLBP4LQP',
                          'domestic':'KCLBP4DMP',
                          'global':'KCLBP4GLP',
                          'cyclical':'KCLBP4CP',
                          'defensive':'KCLBP4DFP',
                          'highvol':'KCLBP4LRP',
                          'lowvol':'KCLBP4HRP'},
            'filename' : 'Breakdown_{date}_v41_KECH.xlsx',
            'sheet_name' : 'Summary_complete',
            'portfolios' : pd.Index(['MorningStar_CD','sales_pct','highvol','lowvol','quality_high','quality_low','growth','value',
                                 'momentum_high','momentum_low', 'high_risk', 'low_risk', 'oppo_high', 'oppo_low']),
            'table_compo':'INDEX_COMPOSITION',
            'table_level':'INDEX_LEVEL',
            'rebalance': 'monthly',
    }

    s_pr_tr = pd.Series(mapping_price_return_total_return)
    s_pr_tr_id = s_pr_tr.rename(index=rep.mapping_index_from_ticker(s_pr_tr.index))
    s_pr_tr_id = s_pr_tr_id.replace(rep.mapping_index_from_ticker(s_pr_tr.values))

    s_ticker_index_id = rep.mapping_index_from_ticker(s_pr_tr.index.union(s_pr_tr.values))
    s_index_id_ticker = pd.Series(index = s_ticker_index_id.values, data = s_ticker_index_id.index)
    idx_id = pd.to_numeric(s_pr_tr_id.index.union(s_pr_tr_id.values), errors = 'coerce').dropna()
    s_index_id_name = rep.mapping_from_index(idx_id, 'short_name')
    s_ticker_name = rep.mapping_index_from_ticker(s_pr_tr.index.union(s_pr_tr.values), 'short_name')

    s_ticker_name = pd.Series(s_ticker_name.to_dict())
    index_id = list(range(109,146)) + [147]
